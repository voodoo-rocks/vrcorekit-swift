//
//  VRTextFieldStateManager.swift
//  CoOpCube
//
//  Created by Alice on 08/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRStyledTextFieldStateManager: NSObject, UITextFieldDelegate {
    var styledField: VRStylable?
    
    @objc public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        styledField!.fieldState = FieldState.editing
        
        if(styledField!.didClearText) != nil {
            styledField!.didClearText!()
        }
        
        return true
    }
    
    @objc public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let result = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if result.characters.count == 0 && styledField!.didClearText != nil {
            styledField!.didClearText!()
        }
        
        if result != textField.text && styledField!.didChangeQuery != nil {
            styledField!.didChangeQuery!(result)
        }
        
        return styledField!.shouldChangeCharactersInRange != nil ? styledField!.shouldChangeCharactersInRange!(result) : true
    }
    
    @objc public func textFieldDidBeginEditing(_ textField: UITextField) {
        styledField!.fieldState = FieldState.editing
        
        if styledField!.didBeginEditing != nil {
            styledField!.didBeginEditing!()
        }
    }
    
    @objc public func textFieldDidEndEditing(_ textField: UITextField) {
        styledField!.fieldState = textField.text!.characters.count > 0 ? FieldState.filled : FieldState.empty
        
        if styledField!.didEndEditing != nil {
            styledField!.didEndEditing!()
        }
    }
    
    @objc public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if styledField!.shouldReturn != nil {
            styledField!.shouldReturn!()
        }
        textField.resignFirstResponder()
        
        return true
    }
}
