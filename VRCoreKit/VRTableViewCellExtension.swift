//
//  TableViewCell.swift
//  CoOpCube
//
//  Created by Alice on 16/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension UITableViewCell {

    public static func cellForTableView(_ tableView: UITableView, indexPath: IndexPath) -> Self {
        return cellForTableViewHelper(tableView, indexPath: indexPath)
    }
    
    fileprivate class func cellForTableViewHelper<T>(_ tableView: UITableView, indexPath: IndexPath) -> T
    {
        let nibName: String = NSStringFromClass(T.self as! AnyClass).components(separatedBy: ".").last!
        return cellForTableViewHelper(tableView, fromNib: nibName)
    }
    
    public static func cellForTableView(_ tableView: UITableView, fromNib: String) -> Self {
        return cellForTableViewHelper(tableView, fromNib: fromNib)
    }
    
    fileprivate class func cellForTableViewHelper<T>(_ tableView: UITableView, fromNib nibName: String) -> T
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: nibName)
        if (cell == nil) {
            let nibs = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
            for nib in nibs! {
                if ((nib as AnyObject).isKind(of: self)) {
                    tableView.register(nib as? UINib, forCellReuseIdentifier: nibName)
                    cell = nib as? UITableViewCell
                    break
                }
            }
        }
        return cell as! T
    }
    
    public func layoutHeightForTableView(tableView: UITableView) -> CGFloat {
        let height = self.contentView.systemLayoutSizeFitting(CGSize(width: tableView.bounds.size.width, height: 0), withHorizontalFittingPriority: UILayoutPriorityRequired, verticalFittingPriority: UILayoutPriorityDefaultLow).height
        return height + 1
    }
}
