//
//  ViewController.swift
//  VRCoreKit
//
//  Created by Alice on 31/10/2016.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class ViewController: UIViewController {

    override open func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

