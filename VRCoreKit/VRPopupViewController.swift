//
//  PopupViewController.swift
//  Restomat
//
//  Created by Alice on 31/01/2017.
//  Copyright © 2017 Voodoo Rocks. All rights reserved.
//

import UIKit

open class VRPopupViewController: VRBaseViewController {
    
    public var didDismiss: (()->())?
    public var hidesNavBar = false {
        didSet {
            navigationController?.isNavigationBarHidden = hidesNavBar
        }
    }
    
    fileprivate var popupManager = VRPopupManager()
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    public func showInPopupMode() {
        let navController = UINavigationController(navigationBarClass: UINavigationBar.self, toolbarClass: nil)
        navController.viewControllers = [self]
        popupManager.popupController(navController)
    }
    
    public func dismiss() {
        popupManager.dismissLastAnimated(false)
        if(didDismiss != nil) {
            didDismiss!()
        }
    }
    
    @IBAction override func onBack() {
        dismiss()
    }
    
}
