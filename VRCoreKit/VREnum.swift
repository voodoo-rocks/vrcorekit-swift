//
//  VREnum.swift
//  CoOpCube
//
//  Created by Alice on 09/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VREnum : VRBaseEntity {
    required override public init() {
        super.init()
    }
    
    public convenience init(withTypeId type: Int) {
        self.init()
        
        defer {
            self.typeId = type //Because willSet and didSet observers are not called when a property is first initialized. They are only called when the property’s value is set outside of an initialization context.
        }
    }
    
    public var typeId: Int = 0 {
        didSet {
            if names.count <= typeId {
                NSException.raise(NSExceptionName(rawValue: "Exception"), format:String(describing: type(of: self)) + " does not have type with index " + String(describing: index), arguments:getVaList([]))
            } else if name != names[typeId] {
                self.name = names[typeId]
            }
        }
    }
    
    public var name: String = "" {
        didSet {
            let index = names.index(of: name)
            if index == nil {
                NSException.raise(NSExceptionName(rawValue: "Exception"), format: "Type" + name + " is not defined for " + String(describing: type(of: self)), arguments:getVaList([]))
            } else if typeId != index {
                self.typeId = index!
            }
        }
    }
    
    public var names = [String]()
}
