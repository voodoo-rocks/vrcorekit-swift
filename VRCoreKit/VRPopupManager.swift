//
//  VRPopupManager.swift
//  Senate
//
//  Created by Egor Gaydamak on 24/10/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRPopupManager: VRSharedObject {
    var controllers: [UIViewController] = []
    
    var parentController: UIViewController?
    var activeController: UIViewController?
    
    var activeView: UIView?
    
    var topmostView: UIView? {
        parentController = UIApplication.shared.keyWindow?.rootViewController
        
        if (parentController?.presentedViewController != nil) {
            parentController = parentController?.presentedViewController
        }
        
        if (parentController == nil) {
            parentController = UIViewController()
            parentController?.view.frame = UIScreen.main.bounds
            parentController?.view.backgroundColor = UIColor.clear
            UIApplication.shared.keyWindow?.rootViewController = parentController
        }
        
        return parentController?.view
    }
    
    public func popupController(_ controller: UIViewController) {
        let topmost = topmostView
        assert(topmost != nil, "There is no view that can be used for showing your UI")
        
        activeController = controller
        activeView = activeController?.view
        
        activeView?.alpha = 0
        
        let popupViewWidth: CGFloat = topmost!.frame.size.width
        let popupViewHeight: CGFloat = topmost!.frame.size.height
        
        let newCenter = CGPoint(x: topmost!.frame.origin.x + topmost!.frame.size.width / 2, y: topmost!.frame.origin.y + topmost!.frame.size.height / 2)
        let newOrigin = CGPoint(x: newCenter.x - popupViewWidth / 2, y: newCenter.y - popupViewHeight / 2)
        
        controller.view.frame = CGRect(x: newOrigin.x, y: newOrigin.y, width: popupViewWidth, height: popupViewHeight);
        topmost?.addSubview(controller.view)
        
        controllers.append(activeController!)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.activeView?.alpha = 1.0
        }) 
    }

    public func dismissLastAnimated(_ animated: Bool) {
        if controllers.count > 0 {
            activeController = controllers.last
            activeView = activeController?.view
            
            activeController?.dismiss(animated: animated, completion: nil)
            
            UIView.animate(withDuration: 0.3, animations: {
                
                if (self.activeView != nil) {
                    self.activeView?.alpha = 0
                }
                
                }, completion: { (finished: Bool) -> Void in
                    if (self.activeView != nil) {
                        self.activeView?.removeFromSuperview()
                    }
                    self.controllers.removeLast()
            })
        }
    }
    
}
