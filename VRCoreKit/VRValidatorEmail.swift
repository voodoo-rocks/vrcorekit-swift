//
//  VRValidatorEmail.swift
//  CoOpCube
//
//  Created by Alice on 14/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRValidatorEmail: NSObject, VRStringValidator {
    
    public var messageOnFail = "Value is not a valid email"
    
    var allowEmptyValue = false
    
    @objc public func validate(_ value: String) -> Bool {
        if value == "" && allowEmptyValue {
            return true
        }
        let emailRegexp = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,25}"
        return value.range(of: emailRegexp, options: .regularExpression) != nil
    }
}
