//
//  File.swift
//  CoOpCube
//
//  Created by Alice on 06/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import Foundation

extension Dictionary
{
    public func keysToCamelCase() -> Dictionary {
        return transformKeys() { string in
            return string.underscoreToCamelCase
        }
    }
    
    public func keysToUnderscoreCase() -> Dictionary {
        return transformKeys() { string in
            return string.camelCaseToUnderscore
        }
    }

    func transformKeys(_ transformKey: (String)->String) -> Dictionary
    {
        let keys = Array(self.keys)
        let values = Array(self.values)
        var dict: Dictionary = [:]
        
        keys.enumerated().forEach { (index, key) in
            
            var value = values[index]
            if let v = value as? Dictionary,
                let vl = v.transformKeys(transformKey) as? Value {
                value = vl
            }
            
            var newKey = key
            
            if let k = key as? String, let ky = transformKey(k) as? Key {
                newKey = ky
            }
            
            dict[newKey] = value
        }
        
        return dict
    }
}

func + <K,V>(left: Dictionary<K,V>, right: Dictionary<K,V>) -> Dictionary<K,V>
{
    var map = Dictionary<K,V>()
    for (k, v) in left {
        map[k] = v
    }
    for (k, v) in right {
        map[k] = v
    }
    return map
}
