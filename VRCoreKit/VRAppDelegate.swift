//
//  VRAppDelegateExtension.swift
//  CoOpCube
//
//  Created by Alice on 14/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRAppDelegate: UIResponder, UIApplicationDelegate
{
    public var window: UIWindow?
    
    public override init() {
        super.init()
        
        self.window = UIWindow();
        self.window!.makeKeyAndVisible();
        self.window!.frame = UIScreen.main.bounds;
    }
    
    public static func shared() -> Self {
        return sharedHelper()
    }
    
    fileprivate class func sharedHelper<T: VRAppDelegate>() -> T! {
        return UIApplication.shared.delegate as! T
    }
    
    public func loadViewControllerAsRoot(_ controller: UIViewController) {
        self.loadViewControllerAsRoot(controller, navigationClass: UINavigationBar.self)
    }
    
    public func loadViewControllerAsRoot(_ controller: UIViewController, navigationClass: UINavigationBar.Type) {
        self.loadViewControllerAsRoot(controller, navigationClass: navigationClass, withAnimationOptions: [])
    }
    
    public func loadViewControllerAsRoot(_ controller: UIViewController, navigationClass: UINavigationBar.Type, withAnimationOptions: UIViewAnimationOptions) {
        let navController = UINavigationController(navigationBarClass: navigationClass, toolbarClass: nil)
        navController.viewControllers = [controller]
        self.window!.rootViewController = navController
        
        UIView.transition(with: self.window!,
                                  duration: 0.4,
                                  options: withAnimationOptions,
                                  animations: {  self.window!.rootViewController = navController },
                                  completion: nil)
    }
}
