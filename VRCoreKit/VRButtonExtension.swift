//
//  VRButtonExtension.swift
//  CoOpCube
//
//  Created by Egor Gaydamak on 08/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension UIButton {
     @IBInspectable public var normalBackgroundColor: UIColor? {
        set {
            self.setBackgroundImage(UIImage(color: newValue!), for: UIControlState.normal)
        }
        get {
            return self.backgroundImage(for: UIControlState.normal)?.averageColor()
        }
    }
    @IBInspectable public var highlightedBackgroundColor: UIColor? {
        set {
            self.setBackgroundImage(UIImage(color: newValue!), for: UIControlState.highlighted)
        }
        get {
            return self.backgroundImage(for: UIControlState.highlighted)?.averageColor()
        }
    }
    @IBInspectable public var selectedBackgroundColor: UIColor? {
        set {
            self.setBackgroundImage(UIImage(color: newValue!), for: UIControlState.selected)
        }
        get {
            return self.backgroundImage(for: UIControlState.selected)?.averageColor()
        }
    }
}
