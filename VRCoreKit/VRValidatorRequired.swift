//
//  VRValidatorRequired.swift
//  CoOpCube
//
//  Created by Alice on 14/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRValidatorRequired: NSObject, VRStringValidator {
    
    public var messageOnFail = "Value is required"
    
    public func validate(_ value: String) -> Bool {
        let trimmedString = value.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmedString.characters.count > 0
    }
}
