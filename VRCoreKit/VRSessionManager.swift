//
//  SessionManager.swift
//  Dogs Day
//
//  Created by Alice on 31/10/2016.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import Foundation

open class VRSessionManager: VRSharedObject {
    private let tokenKey = "SessionManager.Token"
    private let userKey = "SessionManager.User"
    
    public var isAuthorized: Bool {
        get {
            return token != nil
        }
    }
    
    public var token: String? {
        didSet {
            token?.serializeToUserDefaultsForKey(tokenKey)
        }
    }
    
    public required init() {
        super.init()
        token = String(userDefaultsKey:tokenKey)
    }
    
    public func logout() {
        token = nil
        UserDefaults.standard.removeObject(forKey: userKey)
        UserDefaults.standard.removeObject(forKey: tokenKey)
    }
    
}
