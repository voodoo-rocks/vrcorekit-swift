//
//  VRCollectionViewCellExtension.swift
//  CoOpCubeAdmin
//
//  Created by Egor Gaydamak on 15/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    public static func cellForCollectionView(_ collectionView: UICollectionView, indexPath: IndexPath) -> Self {
        return cellForCollectionViewHelper(collectionView, indexPath: indexPath)
    }
    
    fileprivate class func cellForCollectionViewHelper<T>(_ collectionView: UICollectionView, indexPath: IndexPath) -> T
    {
        let nibName: String = NSStringFromClass(T.self as! AnyClass).components(separatedBy: ".").last!
        collectionView.register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: nibName)
        return collectionView.dequeueReusableCell(withReuseIdentifier: nibName, for: indexPath as IndexPath) as! T
    }
    
    public static func cellForCollectionView(_ collectionView: UICollectionView, indexPath: IndexPath, fromNib: String) -> Self {
        return cellForCollectionViewHelper(collectionView, indexPath: indexPath, fromNib: fromNib)
    }
    
    fileprivate class func cellForCollectionViewHelper<T>(_ collectionView: UICollectionView, indexPath: IndexPath, fromNib: String) -> T
    {
        collectionView.register(UINib(nibName: fromNib, bundle: nil), forCellWithReuseIdentifier: fromNib)
        return collectionView.dequeueReusableCell(withReuseIdentifier: fromNib, for: indexPath as IndexPath) as! T
    }
}
