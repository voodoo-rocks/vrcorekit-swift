//
//  VRStyledTextField.swift
//  CoOpCube
//
//  Created by Alice on 08/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRStyledTextField: UITextField, VRStylable
{
    @IBOutlet var inputAccessoryViewOutlet: UIToolbar? {
        didSet {
            inputAccessoryView = inputAccessoryViewOutlet
        }
    }
    
    public var didChangeQuery: ((String) -> ())?
    public var didClearText: (() -> ())?
    public var didBeginEditing: (() -> ())?
    public var didEndEditing: (() -> ())?
    public var shouldReturn: (() -> ())?
    public var shouldChangeCharactersInRange: ((String) -> Bool)?
    
    public var shouldAnimateBackgroundColor: ((FieldState, FieldState) -> ())? // if you set this, you are responsible for setting background color after your animations
    
    @IBInspectable public var leftSideImage: UIImage?
    
    open var stateManager = VRStyledTextFieldStateManager()
    open var styledViewsHelper = VRStyledViewsHelper.shared()
    
    public var fieldState = FieldState.empty {
        didSet {
            textColor = styledViewsHelper.textColors != nil ? styledViewsHelper.textColors![fieldState.rawValue] : textColor
            borderColor = styledViewsHelper.borderColors != nil ? styledViewsHelper.borderColors![fieldState.rawValue] : borderColor
            self.leftView = UIImageView(image: self.leftImageForState(fieldState))
            
            if shouldAnimateBackgroundColor != nil {
                shouldAnimateBackgroundColor!(oldValue, fieldState)
            } else {
                configureBackgroundColor()
            }
        }
    }
    
    public func configureBackgroundColor() {
        backgroundColor = styledViewsHelper.backgroundColors != nil ? styledViewsHelper.backgroundColors![fieldState.rawValue] : backgroundColor
    }
    
    public func configureViewWithStyledViewsHelper(_ helper: VRStyledViewsHelper) {
        styledViewsHelper = helper
        layer.cornerRadius = helper.cornerRadius != nil ? styledViewsHelper.cornerRadius! : self.frame.height / 2
        borderWidth = helper.borderWidth
        tintColor = helper.cursorColor
    }
    
    override open var text: String? {
        willSet {
            fieldState = text?.characters.count != 0 ? FieldState.filled : FieldState.empty
        }
    }
    
    override open func awakeFromNib()
    {
        stateManager.styledField = self
        self.delegate = stateManager
        
        self.leftViewMode = UITextFieldViewMode.always
        
        configureViewWithStyledViewsHelper(styledViewsHelper)
        
        self.fieldState = FieldState.empty
    }
    
    override open func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        if leftView == nil {
            return CGRect.null
        }
        let imageSize = leftView!.frame.size
        let yOffset = bounds.size.height / 2 - imageSize.height / 2
        let xOffset = styledViewsHelper.horizontalContentInset
        return CGRect(x: xOffset, y: yOffset, width: imageSize.width, height: imageSize.height)
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: self.leftSideImage != nil ? styledViewsHelper.textInsetForFieldWithImage : styledViewsHelper.horizontalContentInset, dy: 0)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: self.leftSideImage != nil ? styledViewsHelper.textInsetForFieldWithImage : styledViewsHelper.horizontalContentInset, dy: 0)
    }
    
    func leftImageForState(_ state: FieldState) -> UIImage? {
        if leftSideImage == nil {
            return nil
        }
        if styledViewsHelper.leftImagesColors == nil {
            return leftSideImage // do not repaint
        }
        return leftSideImage!.coloredImage(styledViewsHelper.leftImagesColors![state.rawValue])
    }
    
    override public func highlight() {
        fieldState = FieldState.invalid
    }
}
