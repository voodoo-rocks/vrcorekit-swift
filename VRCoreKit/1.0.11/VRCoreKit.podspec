Pod::Spec.new do |s|
  s.name             = "VRCoreKit"
  s.version          = "1.0.11"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-mobile@bitbucket.org/voodoo-mobile/vrcorekit-swift.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => "public@voodoo-mobile.com" }
  s.source           = { :git => s.homepage, branch:'master', :tag => s.version.to_s }

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'VRCoreKit/VR*.swift'

    s.frameworks = 'Foundation', 'AVFoundation', 'UIKit'
    s.dependency 'SDWebImage', '~> 3.8.2'
    s.dependency 'Alamofire', '~> 4.0.1'

end
