//
//  VRStyledViewsHelper.swift
//  CoOpCube
//
//  Created by Alice on 08/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRStyledViewsHelper: VRSharedObject {
    public var textColors: [UIColor]? // Empty, Filled, Editing, Invalid, Ready
    public var borderColors: [UIColor]?
    public var backgroundColors: [UIColor]?
    public var leftImagesColors: [UIColor]?
    
    public var textInsetForFieldWithImage: CGFloat = 35.0 // inset from left border
    public var horizontalContentInset: CGFloat = 8.0 // inset for image if any or for text if not
    public var verticalContentInset: CGFloat = 5.0 // used in text views (in text fields contents is centered)
    public var cornerRadius: CGFloat? // by default corners are rounded (height / 2)
    public var borderWidth: CGFloat = 0.0
    public var cursorColor = UIColor.black
}
