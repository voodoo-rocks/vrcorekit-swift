//
//  VRColorExtension.swift
//  CoOpCube
//
//  Created by Alice on 16/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension UIColor {
    public convenience init(hexString: String) {
        let string = (hexString.range(of: "0x") != nil) ? hexString : "0x" + hexString
        var hexValue: uint = 0
        Scanner(string: string).scanHexInt32(&hexValue)
        self.init(hexCode:hexValue)
    }
    
    public convenience init(hexCode: uint) {
        self.init(colorLiteralRed: (Float)((hexCode & 0xFF0000) >> 16)/255, green: (Float)((hexCode & 0xFF00) >> 8)/255, blue: (Float)(hexCode & 0xFF)/255, alpha: 1)
    }
}
