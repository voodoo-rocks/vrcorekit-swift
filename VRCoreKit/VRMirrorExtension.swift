//
//  VRMirrorExtension.swift
//  CoOpCubeAdmin
//
//  Created by Alice on 01/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension Mirror {
    func getChildrenRecursive() -> [String:Any] {
        var dict = [String: Any]()
        if String(describing: self.subjectType) == "VREntity" { // don't go that far
            return dict
        }
        
        for child in self.children {
            if let propertyName = child.label {
                dict[propertyName] = child.value
            }
        }
        if let superMirror = self.superclassMirror {
            dict = dict + superMirror.getChildrenRecursive()
        }
        return dict
    }
}
