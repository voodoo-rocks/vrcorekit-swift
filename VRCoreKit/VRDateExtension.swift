//
//  fef.swift
//  CoOpCubeAdmin
//
//  Created by Alice on 02/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension Date {
    
    static func mySqlFormat() -> String {
        return "yyyy-MM-dd HH:mm:ss"
    }
    
    static func sharedDateFormatter() -> DateFormatter {
        struct Holder {
            static var dateFormatter: DateFormatter?
        }
        if Holder.dateFormatter == nil {
            Holder.dateFormatter = DateFormatter()
            Holder.dateFormatter!.locale = Locale(identifier: "en_GB") as Locale!
        }
        return Holder.dateFormatter!
    }
    
    //=============// Date from String //=============//
    
    public static func utcDateFromString(_ string: String?) -> Date? {
        return Date.utcDateFromString(string, format: Date.mySqlFormat())
    }
    
    public static func localDateFromString(_ string: String?) -> Date? {
        return localDateFromString(string, format: Date.mySqlFormat())
    }
    
    public static func utcDateFromString(_ string: String?, format: String) -> Date? {
        return dateFromString(string, format: format, timeZone: TimeZone(identifier: "UTC"))
    }
    
    public static func localDateFromString(_ string: String?, format: String) -> Date? {
        return dateFromString(string, format: format, timeZone: TimeZone.current)
    }
    
    public static func dateFromString(_ string: String?, format: String, timeZone: TimeZone?) -> Date? {
        if string == nil {
            return nil
        }
        let dateFormatter = Date.sharedDateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone as TimeZone!
        return dateFormatter.date(from: string!)
    }
    
    //=============// String from Date //=============//
    
    public func utcDateString() -> String? {
        return utcDateString(Date.mySqlFormat())
    }
    
    public func localDateString() -> String? {
        return localDateString(Date.mySqlFormat())
    }
    
    public func utcDateString(_ format: String) -> String? {
        return stringFromDate(format, timeZone: TimeZone(identifier: "UTC"))
    }
    
    public func localDateString(_ format: String) -> String? {
        return stringFromDate(format, timeZone: TimeZone.current)
    }
    
    func stringFromDate(_ format: String, timeZone: TimeZone?) -> String? {
        let dateFormatter = Date.sharedDateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone as TimeZone!
        return dateFormatter.string(from: self as Date)
    }
}
