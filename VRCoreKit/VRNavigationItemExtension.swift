//
//  VRNavigationItemExtension.swift
//  CoOpCube
//
//  Created by Alice on 16/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension UINavigationItem {
    public func assignRightItemNoOffset(_ item: UIBarButtonItem) {
        self.rightBarButtonItems = [negativeSpacer(), item]
    }
    
    public func assignRightItemsNoOffset(_ items: [UIBarButtonItem]) {
        var itemsToAdd = items
        itemsToAdd.insert(negativeSpacer(), at: 0)
        self.rightBarButtonItems = itemsToAdd
    }
    
    public func assignLeftItemNoOffset(_ item: UIBarButtonItem) {
        self.leftBarButtonItems = [negativeSpacer(), item]
    }
    
    public func assignLeftItemsNoOffset(_ items: [UIBarButtonItem]) {
        var itemsToAdd = items
        itemsToAdd.insert(negativeSpacer(), at: 0)
        self.leftBarButtonItems = itemsToAdd
    }
}

public func negativeSpacer() -> UIBarButtonItem {
    let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
    negativeSpacer.width = -16
    return negativeSpacer
}
