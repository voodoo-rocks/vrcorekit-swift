//
//  VRBaseResposne.swift
//  CoOpCubeAdmin
//
//  Created by Alice on 25/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRException: VREntity {
    public var name: String?
    public var message: String?
    public var code: NSNumber?
}

open class VRBaseResponse: VREntity {
    public var exception: VRException?
    public var success: Bool = false
}
