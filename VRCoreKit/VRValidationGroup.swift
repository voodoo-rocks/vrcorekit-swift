//
//  VRValidationGroup.swift
//  CoOpCube
//
//  Created by Alice on 10/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRValidationGroup: NSObject {
    @IBOutlet public var fieldsForValidation: [VRStringValidatable]?
    @IBOutlet public var validator: VRStringValidator?
    @IBOutlet public var validationForm: VRValidationForm?
    {
        didSet {
            if !(validationForm?.validationGroups.contains(self))! {
                validationForm!.validationGroups.append(self)
            }
        }
    }
    
    @IBInspectable public var shouldDisplayFlash = true
    @IBInspectable public var shouldFocusOnField = true
    @IBInspectable public var shouldHighlightInvalidFields = true
    
    open func validate() -> Bool {
        if validator == nil || fieldsForValidation == nil || validationForm == nil {
            return false
        }
        
        var result = true
        for validatable in fieldsForValidation! {
            if (validatable as! UIView).isHidden {
                continue // do not validate hidden fields
            }
            let oldResult = result
            let isValid = (validator?.validate(validatable.textForValidation))!
            result = result && isValid
            
            if !isValid {
                
                if oldResult && shouldDisplayFlash {
                    VRFlashManager.shared().showFlash(validatable.messageOnFail != nil ? validatable.messageOnFail! : validator!.messageOnFail)
                }
                
                if let delegate = self.validationForm?.delegate {
                    delegate.didFailValidation?(validatable, forValidator: validator!)
                }
                
                if oldResult && shouldFocusOnField {
                    validatable.focus()
                }
                
                if shouldHighlightInvalidFields {
                    validatable.highlight()
                }
            }
        }
        return result
    }
}

func ==(lhs: VRValidationGroup, rhs: VRValidationGroup) -> Bool {
    return lhs == rhs
}
