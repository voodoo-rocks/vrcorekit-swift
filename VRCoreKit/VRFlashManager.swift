//
//  VRFlashManager.swift
//  CoOpCubeAdmin
//
//  Created by Alice on 05/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRFlashManager: VRSharedObject {
    
    public var customNibName: String? = "VRFlashView"
    
    public func showFlash(_ title: String) {
        
        var flash: VRFlashView
        let nibExists = customNibName != nil && Bundle.main.path(forResource: customNibName!, ofType:"nib") != nil
        if nibExists {
            flash = Bundle.main.loadNibNamed(customNibName!, owner: self, options: nil)?.first as! VRFlashView
        } else {
            flash = VRFlashView()
        }
        flash.fromNib = nibExists
        
        flash.title = title
        flash.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height, width: flash.frame.width, height: flash.frame.height)
        UIApplication.shared.keyWindow?.addSubview(flash)
        flash.dismissWithDelay(delay: 4)
    }
}
