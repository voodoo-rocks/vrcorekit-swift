//
//  VRBaseRequest.swift
//  CoOpCubeAdmin
//
//  Created by Alice on 25/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRMetaData: VREntity {
    
    var timezone: NSInteger {
        get {
            return TimeZone.current.secondsFromGMT() / 60
        }
    }
    var version: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    var bundle: String {
        return Bundle.main.bundleIdentifier!
    }
    var udid: String {
        return UIDevice.current.identifierForVendor!.uuidString
    }
    var method: String = ""
}

open class VRBaseRequest: VREntity {
    
    var meta: VRMetaData = VRMetaData()
    let baseUrl = Bundle.main.object(forInfoDictionaryKey: "baseUrl") as? String
    
    var payload: [String: Any] {
        get {
            return toDictionary()
        }
    }
    
    public override init() {
        super.init()
    }
    
    required public init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)
    }
    
    public override init(userDefaultsKey: String) {
        super.init(userDefaultsKey: userDefaultsKey)
    }
    
    public init(method: String!) {
        super.init()
        setMethod(method: method)
    }
    
    public func setMethod(method: String!) {
        meta.method = baseUrl != nil ? baseUrl! + method : method
    }
    
    public func method() -> String {
        return meta.method
    }
}
