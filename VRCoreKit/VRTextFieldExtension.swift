//
//  ef.swift
//  CoOpCube
//
//  Created by Alice on 10/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

private var xoAssociationKey: UInt8 = 0

extension UITextField : VRStringValidatable {
    public var textForValidation: String {
        get {
            return self.text!
        }
    }
    @IBInspectable public var messageOnFail: String? {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? String
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    public func focus() {
        self.becomeFirstResponder()
    }
    
    public func highlight() {
    }
}
