//
//  File.swift
//  CoOpCube
//
//  Created by Alice on 02/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRBaseViewController: UIViewController
{
    @IBInspectable public var screenTitle: String {
        set {
            self.navigationItem.title = newValue
        }
        get {
            return self.navigationItem.title!
        }
    }
    
    public var willShowKeyboard: ((_ kbHeight: CGFloat) -> ())?
    public var willHideKeyboard: ((_ kbHeight: CGFloat) -> ())?
    
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.unregisterForKeyboardNotifications()
    }
    
    
    public func navigate(_ controller: UIViewController) {
        dismissKeyboard()
        self.navigationController!.pushViewController(controller, animated: true);
    }
    
    public func navigateBack() {
        dismissKeyboard()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onBack() {
        navigateBack()
    }
    
    
    public func setKeyboardHandling() {
        self.setKeyboardHandling(self.view as! UIScrollView)
    }
    
    public func setKeyboardHandling(_ scrollView: UIScrollView, insets: UIEdgeInsets = UIEdgeInsets.zero, selector: Selector = #selector(dismissKeyboard)) {
        scrollView.contentInset = insets
        
        let tap = UITapGestureRecognizer(target: self, action: selector)
        
        self.willShowKeyboard = { (kbHeight: CGFloat) in
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: kbHeight, right: 0)
            self.view.addGestureRecognizer(tap)
        }
        
        self.willHideKeyboard = { (kbHeight: CGFloat) in
            scrollView.contentInset = insets
            self.view.removeGestureRecognizer(tap)
        }
    }
    
    public func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unregisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillBeShown(notification: NSNotification) {
        let kbSize = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        if self.willShowKeyboard != nil {
            self.willShowKeyboard!(kbSize.height)
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        let kbSize = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        if self.willHideKeyboard != nil {
            self.willHideKeyboard!(kbSize.height)
        }
    }
}
