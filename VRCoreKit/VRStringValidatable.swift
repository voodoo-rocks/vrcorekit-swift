//
//  dw.swift
//  CoOpCube
//
//  Created by Alice on 10/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import Foundation

@objc public protocol VRStringValidatable {
    var textForValidation: String {get}
    var messageOnFail: String? {set get}
    
    func focus()
    
    func highlight()
}
