//
//  File.swift
//  CoOpCube
//
//  Created by Alice on 03/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

protocol VRSwitcher {
    var selectedIndex: Int {get set}
    var didChangeIndex: ((Int) -> Void)? {get set}
}
