//
//  VRButtonsSwitcher.swift
//  CoOpCube
//
//  Created by Alice on 03/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRButtonsSwitcher: NSObject, VRSwitcher
{
    @IBOutlet public var buttons: [UIButton]! {
        willSet {
            for button in newValue {
                button.addTarget(self, action: #selector(onButton), for: UIControlEvents.touchUpInside)
            }
        }
    }

    func onButton(_ sender: UIButton) {
        self.selectedIndex = buttons.index(of: sender)!
    }
    
    public var selectedIndex: Int = -1 {
        willSet {
            if newValue == selectedIndex {
                return;
            }
            if selectedIndex != -1 {
                buttons[selectedIndex].isSelected = false;
            }
            self.buttons[newValue].isSelected = true;
            
            if didChangeIndex != nil {
                didChangeIndex!(newValue)
            }
        }
    }
    
    public var didChangeIndex: ((Int) -> Void)?
}


























