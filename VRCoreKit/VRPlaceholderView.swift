//
//  File.swift
//  CoOpCube
//
//  Created by Alice on 03/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRPlaceholderView: UIView
{
    @IBOutlet weak var parentController: UIViewController?
    @IBOutlet public var controller: UIViewController? {
        willSet {
            if self.controller != nil {
                self.controller!.view.removeFromSuperview()
            }
        }
        didSet {
            self.showController()
        }
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        self.showController()
    }
    
    func showController() {
        if controller != nil && controller!.view != nil {
            controller!.view.frame = self.bounds
            self.addSubview(controller!.view)
            if (self.parentController != nil) {
                self.parentController!.addChildViewController(controller!)
            }
        }
    }
}
