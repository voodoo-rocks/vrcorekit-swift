//
//  LocationManager.swift
//  CoOpCube
//
//  Created by Egor Gaydamak on 04/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit
import CoreLocation

open class VRLocationManager: VRSharedObject, CLLocationManagerDelegate {
    
    var systemLocationManager = CLLocationManager()
    let geoCoder = CLGeocoder()
    var changeLocationBlock: (CLLocationCoordinate2D)->() = {_ in }
    public var lastKnownLocation: CLLocation?
    
    public func currentCoordinateWithCompletion(_ callback: @escaping (CLLocationCoordinate2D)->()) {
        changeLocationBlock = callback
        
        systemLocationManager.delegate = self
        systemLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        systemLocationManager.requestWhenInUseAuthorization()
        systemLocationManager.startUpdatingLocation()
    }
    
    public func lastKnownCoordinate() -> CLLocationCoordinate2D? {
        if lastKnownLocation == nil {
            currentCoordinateWithCompletion {_ in}
            return nil
        }
        return (lastKnownLocation!.coordinate)
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        
        if locations.last != nil {
            lastKnownLocation = locations.last!
            changeLocationBlock(locations.last!.coordinate)
            changeLocationBlock = {_ in }
        } else {
            print("Error while updating location")
            VRFlashManager.shared().showFlash("Sorry location can not be determined at the time")
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
        VRFlashManager.shared().showFlash("Sorry location can not be determined at the time")
    }
    
    public func coordinateForAddress(_ address: String, callback: @escaping (CLLocationCoordinate2D)->()) {
        geoCoder.geocodeAddressString(address) { (placemarks: [CLPlacemark]?, error: Error?) in
            if let coordinate = placemarks?[0].location?.coordinate {
                callback(coordinate)
            }
        }
    }
}
