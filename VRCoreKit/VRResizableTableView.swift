//
//  VRResizableTableView.swift
//  Senate
//
//  Created by Alice on 13/10/2016.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import Foundation
import UIKit

open class VRResizableTableView: UITableView {
    
    override open var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIViewNoIntrinsicMetric, height: self.contentSize.height + self.contentInset.top + self.contentInset.bottom);
    }
    
    override open func reloadData()
    {
        super.reloadData()
        invalidateIntrinsicContentSize()
        self.superview!.layoutIfNeeded()
    }
}
