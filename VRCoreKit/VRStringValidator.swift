//
//  VRValidator.swift
//  CoOpCube
//
//  Created by Alice on 10/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

@objc public protocol VRStringValidator {
    var messageOnFail: String { set get }
    func validate(_ value: String) -> Bool
}
