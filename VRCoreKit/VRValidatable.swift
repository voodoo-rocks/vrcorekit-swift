//
//  VRValidatable.swift
//  VRCoreKit
//
//  Created by Alice on 14/02/2017.
//  Copyright © 2017 Voodoo Mobile. All rights reserved.
//

import UIKit
import ReactiveKit
import ObjectiveC

public protocol VRValidatableProtocol {
    var validators: [VRValidator] {set get}
    func validate() -> Bool
}

open class VRValidatable<T>: VRValidatableProtocol, VRStorableToDictionary {
    
    private var value: T?
    public var validators = [VRValidator]()
    
    convenience required public init(_ args: VRValidator ...) {
        self.init()
        validators = args
    }
    
    public func validate() -> Bool {
        return validators.map{ $0.validate(value) }.reduce(true, {$0 && $1})
    }
    
    
    public func toDictionaryValue() -> Any? {
        return self.value
    }
    
    public func fromDictionaryValue(_ value: Any?) {
        self.value = value as! T?
    }
}


fileprivate var AssociatedObjectHandle: UInt8 = 0

extension Property: VRValidatableProtocol {
    
    public var validators: [VRValidator] {
        get {
            let object = objc_getAssociatedObject(self, &AssociatedObjectHandle)
            return object == nil ? [] : object as! [VRValidator]
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    convenience public init(_ defaultValue: Value, _ firstArg: VRValidator, _ args: VRValidator ...) {
        self.init(defaultValue)
        validators = args + [firstArg]
    }
    
    public func validate() -> Bool {
        return validators.map{ $0.validate(value) }.reduce(true, {$0 && $1})
    }
}
