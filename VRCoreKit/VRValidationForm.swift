//
//  VRValidationForm.swift
//  CoOpCube
//
//  Created by Alice on 10/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRValidationForm : NSObject {
    var validationGroups = [VRValidationGroup]()
    var delegate: VRValidationDelegate?
    
    public func validate() -> Bool {
        var result = true;
        for group in validationGroups {
            result = result && group.validate()
        }
        return result
    }
}
