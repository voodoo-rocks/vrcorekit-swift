//
//  ImageExtention.swift
//  DogsDay
//
//  Created by Alice on 08/02/2017.
//  Copyright © 2017 Voodoo Mobile. All rights reserved.
//

import UIKit

extension UIImage {
    
    public func toEncodedString() -> String {
       return UIImageJPEGRepresentation(self, 0.85)!.base64EncodedString(options: [])
    }
}
