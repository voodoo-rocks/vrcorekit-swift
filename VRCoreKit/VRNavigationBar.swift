//
//  VRNavigationBar.swift
//  CoOpCube
//
//  Created by Alice on 15/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRNavigationBar: UINavigationBar {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.customize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.customize()
    }
    
    open func customize() {}
}
