//
//  VRCollectionViewExtension.swift
//  CoOpCubeAdmin
//
//  Created by Alice on 04/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    public func sizeForItemsCountInRow(_ itemsInRow: Int) -> CGSize {
        return sizeForItemsCountInRow(itemsInRow, sideMultiplier: 1.0)
    }
    
    public func sizeForItemsCountInRow(_ itemsInRow: Int ,sideMultiplier: CGFloat) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let floatItemsCount = CGFloat(itemsInRow)
        let interSpace = flowLayout.minimumInteritemSpacing * (floatItemsCount - 1)
        return CGSize(width: (bounds.width - interSpace)/floatItemsCount, height: ((bounds.width - interSpace)/floatItemsCount) / sideMultiplier)
    }
}
