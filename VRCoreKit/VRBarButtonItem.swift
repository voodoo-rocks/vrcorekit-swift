//
//  VRBarButtonItem.swift
//  CoOpCube
//
//  Created by Alice on 15/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRBarButtonItem: UIBarButtonItem {
    var callback: (()->())?
    
    public init(image: UIImage, highlightedImage: UIImage, callback: (()->())?) {
        super.init()
        self.callback = callback
        
        let button = UIButton()
        button.setImage(image, for: UIControlState.normal)
        button.setImage(highlightedImage, for: UIControlState.highlighted)
        button.addTarget(self, action: #selector(VRBarButtonItem.onClick), for: UIControlEvents.touchUpInside)
        button.sizeToFit()
        
        self.customView = button
    }

    
    public init(image: UIImage, callback: (()->())?) {
        super.init()
        self.callback = callback
        
        let button = UIButton()
        button.setImage(image, for: UIControlState.normal)
        button.addTarget(self, action: #selector(VRBarButtonItem.onClick), for: UIControlEvents.touchUpInside)
        button.sizeToFit()
        
        self.customView = button
    }
    
    public init(title: String, fontName: String, fontSize: CGFloat, defaultColor: UIColor, selectedColor: UIColor, callback: (()->())?) {
        super.init()
        self.callback = callback
        
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.titleLabel!.font = UIFont(name: fontName, size: fontSize)
        button.setTitleColor(defaultColor, for: .normal)
        button.setTitleColor(selectedColor, for: .selected)
        button.addTarget(self, action: #selector(VRBarButtonItem.onClick), for: UIControlEvents.touchUpInside)
        button.sizeToFit()
        
        self.customView = button
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func onClick() {
        if callback != nil {
            callback!()
        }
    }
}
