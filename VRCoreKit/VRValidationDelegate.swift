//
//  VRValidationDelegate.swift
//  CoOpCube
//
//  Created by Alice on 10/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

@objc protocol VRValidationDelegate {
    @objc optional func didFailValidation(_ validatable: VRStringValidatable, forValidator: VRStringValidator)
    @objc optional func didPassValidation(_ validatable: VRStringValidatable, forValidator: VRStringValidator)
}
